from random import randint
from os import stat
from time import strftime
import discord
from discord.ext import commands

from functions import load_variables, fractal_timer, dbg, max_file_size, send_message, embed_help
# Loads variables
lv = load_variables()  # Loaded Variables
maximagesize, colors, prefix, token, activationtime, statuslist = (x for x in lv)
intents = discord.Intents.default()
intents.members = True
intents.message_content = True

bot = commands.Bot(command_prefix=prefix, intents=intents)
dbg(f"Bot's prefix is {prefix}")


# Help command
class MyNewHelp(commands.MinimalHelpCommand):
    # !help
    async def send_bot_help(self, mapping) -> None:
        destination = self.get_destination()
        await destination.send(embed=embed_help(colors))

    # !help [command]
    async def send_command_help(self, cmd) -> None:
        destination = self.get_destination()
        await destination.send(embed=embed_help(colors, str(cmd), prefix))

    # Handle errors
    async def send_error_message(self, error) -> None:
        embed = discord.Embed(title="Error", description=error, color=discord.Color.red())
        destination = self.get_destination()
        await destination.send(embed=embed)


bot.help_command = MyNewHelp()


# Fractal Command
@bot.command()
async def fractal(ctx, *args) -> None:
    author, send, react = ctx.author, ctx.channel.send, ctx.message.add_reaction
    booster_count = ctx.guild.premium_subscription_count if ctx.guild else 0
    await react("👍")

    # Everybody named Arie is cute. That is a fact
    if "Arie" in str(author):
        await send("💜 You're cute Arie 💙")

    if len(args) >= 3:  # See if enough arguments are provided
        try:
            imgsize, re, im = int(args[0]), float(args[1]), float(args[2])
            color = False  # Default

            if args[-1] in ("color=true", "colour=true"):
                color = True

            await send_message(
                f"{author} requested a fractal. Size {imgsize}px, re {re}, im {im}, color={color}",
                send
            )

            if maximagesize == 0 or maximagesize >= imgsize:
                status = discord.Game(f"Making a fractal since {strftime('%H:%M:%S')}")
                await bot.change_presence(status=discord.Status.online, activity=status)
                timetaken = fractal_timer(imgsize, re, im, color)  # Time fractal creation
                filesizemb = round(stat('fractal.png').st_size / (1024 * 1024), 3)
                # Print Time Taken
                dbg(f"Time taken {timetaken}s")
                dbg(f"File Size: {filesizemb}mb")
                # Let user know if fractal is larger than server file upload limit
                filesizelimit = max_file_size(booster_count)
                if filesizemb >= filesizelimit:
                    await send_message(
                        f"{author}'s fractal's file size is {filesizemb}mb, which is greater than this discord server's limit of {filesizelimit}.",
                        send
                    )
                else:
                    await send(
                        f"Time taken: {timetaken}s \nFile Size: {filesizemb}mb",
                        file=discord.File("fractal.png")
                    )
                    await react('✅')
            else:
                await send_message(
                    f"{imgsize} is larger than the limit of {maximagesize}. You can change it in `options.yml`.",
                    send
                )
                await react("❌")

        except Exception as err:
            # Print Errors
            await react("❌")
            await send_message(
              f"{author}'s fractal: {err}",
              send
            )
    else:
        dbg(f"{author} tried to create a fractal without giving all required arguments")
        await send(
            f"{args} does not include all required arguments. Type {prefix}help for more info"
        )
    await change_status("")


# Prints information once bot connects to discord
@bot.event
async def on_ready():
    dbg("Logged in as")
    dbg(f"Name: {bot.user.name}")
    dbg(f"Id: {bot.user.id}")
    await change_status("")


# Change discord status
async def change_status(status):
    status = discord.Game(f"{status} {statuslist[randint(0,1)]}")
    await bot.change_presence(status=discord.Status.online, activity=status)


# Tries to start bot
try:
    bot.run(token)
except Exception as error:
    dbg("Token unreadable!")
    dbg(error)
