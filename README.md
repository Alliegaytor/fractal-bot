# **Fractal Bot**

### **Installation**

Requires Python >= 3.8

`git clone https://gitlab.com/Alliegaytor/fractal-bot`

`cd fractal-bot`

`python3 -m pip install -r requirements.txt`

You may also need to install `llvm-10` if you're on linux

**Once installed you must add your token into options.yml**

Then run `python3 run.py`



### **Usage**

Once your bot is on your server and running run this command:

`!fractal imgSize realValue imaginaryValue`

You can also specify it to use colors with `color=true` appended to the end of the command.

imgSize can be any positive integer, and realValue and imaginaryValue can be any number.

By default the bot will not create fractals larger than 1700px. This is changeable in the **options.yml** file.

Example commands:

`!fractal 1600 -0.74543 0.11301`


`!fractal 1600 0.26 0.0016 color=true`



### **What does this bot do?**

This bot creates [**fractals**](https://en.wikipedia.org/wiki/Fractal) based on user input through discord. Please note that this code takes a while to make fractals for a larger canvas.
You can read more about it over here https://en.wikipedia.org/wiki/Julia_set


Commits welcome.