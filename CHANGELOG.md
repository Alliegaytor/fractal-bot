# Fractal-bot Changelog

## [1.1.5] - 2022.11.27
### Added
- Added intents
- Print ∞ when maximagesize is 0

### Changed
- Update readme
- Code style is now more readable
- No longer use git version of 'discord.py'
- Compacted make_fractal() (reduced characters 811  -> 424)

### Fixed
- ∞ is printed when maximagesize is 0
- Upgraded python version requirements (from 3.6+ to 3.8+) as per discord.py

## [1.1.4] - 2022.02.07
### Changed
- Changed statuses to statuslist
- Minor tweaks to output
- Shrunk embed image from 301kb --> 14.3kb

### Fixed
- Use version_info instead of python_version in run.py (fixes "Please run this program in python 3.6+" for python 3.10+)
- Updated booster requirements for increased file size

## [1.1.3] - 2021.04.16
### Added
- Additional debugging
- Detection for non-American spelling of 'color'
- Detection for boosted servers, and their file size limits
- send_message(), which condenses dbg() and send()

### Changed
- dbg() now is a function that prints
- Renamed functions (from foobar -> foo_bar)

## [1.1.2] - 2021.01.11
### Added
- Print statements indicating if file size of fractal.png is > 8mb

### Changed
- Updated fractal creation message
- Updated way fractal is timed
- Renamed variables and functions to follow PEP 8
- Updated readme to include info about installing llvm-10

### Fixed
- pyyamal included in requirements.txt

## [1.1.1] - 2020.11-01
### Fixed
- Fractals not being created

## [1.1.0] - 2020.11-01
### Added
- Option to change embed colors
- Option to change bot prefix
- Function to read options

### Changed
- Renamed emote() to react()
- Reorganize print statements
- Updated statuses
- Update comments
- Rewrote bot.py to use commands
- Updated README
- Moved fractal() to functions.py

### Removed
- Removed status "Arie is cute!"

### Fixed
- Spelling

## [1.0.1] - 2020-10-24
### Added
- Added errors messages to be printed for run.py's import bot and bot.py's token detection
- Reacted to convey if the fractal command worked or not
- Detection for username 'Arie' and prints that they are cute
- Value of color is printed to log now

### Changed
- Changed the name of some functions

### Fixed
- Increased maximumImageSize from 1600 to 1700 (properly)
- Bot (rarely) reacting to the wrong message or posting fractals in the wrong chat

## [1.0.0] - 2020-10-19
### Added
- Color support for fractals
- Changelog
- Gitignore for test_*.py files

### Changed
- Help command updated with color support
- Switched from putpixel() to load() making fractal generation ~34% faster in testing
- Increased maximumImageSize from 1600 to 1700
