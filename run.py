#!/usr/bin/env python
from functions import dbg
from sys import version_info


def start_program() -> None:
    major_ver, minor_ver, micro_ver = (x for x in version_info[0:3])
    version = f"{major_ver}.{minor_ver}.{micro_ver}"

    dbg("Starting...")
    dbg("Checking for python 3.8+")
    if major_ver == 3 and minor_ver >= 8:
        dbg(f"Python {version} ✓")
        dbg("Starting bot")
        try:
            import bot
        except Exception as e:
            dbg("bot.py could not be imported")
            dbg(e)
    else:
        dbg(f"Python {version} ✖️")
        dbg("Please run this program in python 3.8+")


if __name__ == "__main__":
    start_program()
