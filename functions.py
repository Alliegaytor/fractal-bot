from numba import jit
from PIL import Image
from time import time
from random import randint, choice
from discord import Embed


# Debug print
def dbg(output) -> None:
    from time import strftime
    print(f"[{strftime('%H:%M:%S')}] {output}")


# Send Message through console and discord
async def send_message(message, send) -> None:
    dbg(message)
    await send(message)


# loadVariables function
def load_variables() -> tuple[int, list[int] | None, str, str, str, list[str]]:
    import yaml
    from time import strftime

    # Tries to open options file and saves it as a dictionary
    try:
        with open("options.yml", "r") as stream:
            options = yaml.safe_load(stream)

    except:
        dbg("Could not find or open options.yml")

    # Tries to grab maxImgSize, sets it to 1700 if it fails
    try:
        maximagesize = int(options.get("maximumImageSize"))
        if maximagesize > 0:
            dbg(f"Maximum Image Size: {maximagesize}")
        else:
            dbg("Maximum Image Size: ∞")

    except:
        maximagesize = 1700
        dbg(f"Maximum Image Size unreadable, setting it to {maximagesize} px")

    # Tries to grab embed colors, if it can't it sets it to random
    try:
        colors = [int(i) for i in options.get("embedColor").split()]
    except:
        dbg("Colors are unable to be converted to numbers! Setting to random")
        colors = None

    prefix = str(options.get("botPrefix")) or '!'

    token = str(options.get("token"))
    dbg(f"Token: {token}")

    activationtime = strftime('%d %b | %H:%M:%S')
    status = [f"Creating Fractals since {activationtime}", f"Type {prefix} for info"]
    return maximagesize, colors, prefix, token, activationtime, status


# Returns embed for "{botPrefix}help"
def embed_help(colors, cmd="", prefix="") -> Embed:
    if colors is None:
        embedcolor = randint(0, 16777215)  # Pick random color
    else:
        embedcolor = choice(colors)  # Pick color from colors
    embed = Embed(
        title="",
        url="https://gitlab.com/Alliegaytor/fractal-bot",
        description="version 1.1.5",
        color=embedcolor
    )
    embed.set_author(
        name="Fractal Bot",
        url="https://gitlab.com/Alliegaytor/fractal-bot",
        icon_url="https://i.imgur.com/YJb1e8n.png"
    )

    if cmd == "help":
        embed.add_field(
            name="help",
            value=f"`{prefix}help [command]`   \
            \nProvides help for each command.",
            inline=True
        )
        return embed
    elif cmd == "fractal":
        embed.add_field(
            name="fractal",
            value=f"`{prefix}fractal [imgSize] [reValue] [imValue] [color]`   \
            \nCreates a julia set with the values provided. Color is optional.",
            inline=True
        )
        return embed

    embed.add_field(
        name="Fractal",
        value=f"Creates a fractal based on your input. Color is optional.\
        \nCommands:\n-fractal\n-help",
        inline=True
    )
    embed.set_footer(text="Thanks for using fractal bot")
    return embed


# Time fractal
def fractal_timer(imgsize, re, im, color) -> float:
    start = time()  # Start
    make_fractal(imgsize, re, im, color)  # Fractal creation
    end = time()  # Finish
    return round(end - start, 3)


# Calculate file size limit of server
def max_file_size(booster_count) -> int:
    if booster_count >= 14:
        return 100
    elif booster_count >= 7:
        return 50
    else:
        return 8


# Make fractal
def make_fractal(imgsize, re, im, color) -> None:
    @jit
    def pixel(point, re, im) -> int:
        limit = 500
        thresh = 2
        z = complex(re, im)
        val = point*point + z
        for i in range(1, limit):
            if abs(val) > thresh:
                return i
            val = val*val + z
        return limit

    max = 1.75
    d = (max * 2)/imgsize
    bitmap = Image.new('RGB', (imgsize, imgsize), "white") if color else Image.new('L', (imgsize, imgsize))
    pix = bitmap.load()
    for x in range(imgsize):
        zx = x * d - max
        for y in range(imgsize):
            zy = y * d - max
            z = complex(zx, zy)
            i = pixel(z, re, im)
            pix[x, y] = (i << 21) + (i << 10) + i*8 if color else i

    bitmap.save('fractal.png')
